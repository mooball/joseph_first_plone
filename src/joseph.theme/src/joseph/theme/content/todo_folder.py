# -*- coding: utf-8 -*-
from plone.app.textfield import RichText
from plone.autoform import directives
from plone.dexterity.content import Container
from plone.namedfile import field as namedfile
from plone.supermodel import model
from plone.supermodel.directives import fieldset
from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import implementer
from joseph.theme import _
from plone import api


class ITodoFolder(model.Schema):
    """ Marker interface and Dexterity Python Schema for TodoFolder
    """

    # directives.widget(level=RadioFieldWidget)
    # level = schema.Choice(
    #     title=_(u'Sponsoring Level'),
    #     vocabulary=LevelVocabulary,
    #     required=True
    # )

    text = RichText(
        title=_(u'Text'),
        required=False
    )

    url = schema.URI(
        title=_(u'Link'),
        required=False
    )

    color = schema.Choice(
        title=_(u'Task color'),
        vocabulary='joseph.theme.TaskColors',
        required=False
    )

    assigned_to = schema.Choice(
        title=_(u'Assigned to'),
        vocabulary='plone.app.vocabularies.Users',
        required=True
    )

    due_date = schema.Datetime()

    # fieldset('Images', fields=['logo', 'advertisement'])
    # logo = namedfile.NamedBlobImage(
    #     title=_(u'Logo'),
    #     required=False,
    # )

    # advertisement = namedfile.NamedBlobImage(
    #     title=_(u'Advertisement (Gold-sponsors and above)'),
    #     required=False,
    # )

    # directives.read_permission(notes='cmf.ManagePortal')
    # directives.write_permission(notes='cmf.ManagePortal')
    # notes = RichText(
    #     title=_(u'Secret Notes (only for site-admins)'),
    #     required=False
    # )


@implementer(ITodoFolder)
class TodoFolder(Container):
    """
    """

    def get_user_login(self):
        current_user = api.user.get_current()
        if hasattr(current_user, 'getUserId'):
            current_user = current_user.getUserId()
            print current_user
        else:
            current_user = 'Unknown'

        # user = self.assigned_to

        if current_user == 'clangr':
            return 'This task is assigned to you, Clarissa'
        elif current_user == 'erinc':
            return 'This task is assigned to you, Erin'
        elif current_user == 'keldoi':
            return 'This task is assigned to you, Michael'
        else:
            return "ERROR 404: No Task Assigned To You!"

    # def get_search_results(self):
    #     catalog = api.portal.get_tool('portal_catalog')
    #     return catalog(
    #         portal_type='joseph.theme.TodoTask',
    #         sort_on='effective',
    #         sort_order='reverse',
    #         SearchableText=self.request.form.get(u'text', '')
    #     )
