# -*- coding: utf-8 -*-

__docformat__ = "epytext"

from plone.app.textfield import RichText
from plone.autoform import directives
from plone.dexterity.content import Item
from plone.namedfile import field as namedfile
from plone.supermodel import model
from plone.supermodel.directives import fieldset
from z3c.form.browser.radio import RadioFieldWidget
from zope import schema
from zope.interface import implementer
from joseph.theme import _
from plone import api


class ITodoTask(model.Schema):
    """ Marker interface and Dexterity Python Schema for TodoTask
    """

    # directives.widget(level=RadioFieldWidget)
    # level = schema.Choice(
    #     title=_(u'Sponsoring Level'),
    #     vocabulary=LevelVocabulary,
    #     required=True
    # )

    text = RichText(
        title=_(u'Text'),
        required=False
    )

    url = schema.URI(
        title=_(u'Link'),
        required=False
    )

    assigned_to = schema.Choice(
        title=_(u'Assigned to'),
        vocabulary='plone.app.vocabularies.Users',
        required=True
    )

    color = schema.Choice(
        title=_(u'Task color'),
        vocabulary='joseph.theme.TaskColors',
        required=False
    )

    fruits = schema.Choice(
        title=_(u'Select Fruit'),
        vocabulary='joseph.theme.FruitsSelection',
        required=False
    )

    due_date = schema.Datetime()

    # fieldset('Images', fields=['logo', 'advertisement'])
    # logo = namedfile.NamedBlobImage(
    #     title=_(u'Logo'),
    #     required=False,
    # )

    # advertisement = namedfile.NamedBlobImage(
    #     title=_(u'Advertisement (Gold-sponsors and above)'),
    #     required=False,
    # )

    # directives.read_permission(notes='cmf.ManagePortal')
    # directives.write_permission(notes='cmf.ManagePortal')
    # notes = RichText(
    #     title=_(u'Secret Notes (only for site-admins)'),
    #     required=False
    # )


@implementer(ITodoTask)
class TodoTask(Item):

    # Checking of fruits
    def define_fruit(self):
        if self.fruits == 'apple':
            return 'You have chosen apple.' \
                ' An apple a day keeps the doctor away'
        elif self.fruits == 'banana':
            return 'You have chosen banana. Rich in potassium'
        elif self.fruits == 'blueberry':
            return 'You have chosen blueberry. WOW!'
        else:
            return 'ERROR 404: Fruit not found'

    def get_user_login(self):
        current_user = api.user.get_current()
        if hasattr(current_user, 'getUserId'):
            current_user = current_user.getUserId()
            print current_user
        else:
            current_user = 'Unknown'

        user = self.assigned_to
        print user

        if user == 'clangr':
            return 'This task is assigned to Clarissa'
        elif user == 'erinc':
            return 'This task is assigned to Erin'
        elif user == 'kelv':
            return 'This task is assigned to Michael'
        else:
            return "ERROR 404: No Task Assigned To You!"
