# -*- coding: utf-8 -*-

__docformat__ = "epytext"

from Products.CMFPlone.interfaces import INonInstallable
from zope.interface import implementer
from Products.CMFCore.utils import getToolByName


@implementer(INonInstallable)
class HiddenProfiles(object):

    def getNonInstallableProfiles(self):
        """Hide uninstall profile from site-creation and quickinstaller."""
        return [
            'joseph.theme:uninstall',
        ]


def add_catalog_indexes(site):
    catalog = getToolByName(site, 'portal_catalog')
    indexes = catalog.indexes()
    assignment = (
        ("assigned_to", "FieldIndex"),
        ("color", "FieldIndex"),
        ("due_date", "DateIndex")
    )
    for name, meta_type in assignment:
        if name not in indexes:
            catalog.addIndex(name, meta_type)
            print "Added %s for field %s." % (meta_type, name)


def setup_various(context):
    theme = "joseph.theme.marker.txt"
    # file = open(theme, 'r')
    if context.readDataFile(theme) is None:
        return

    portal = context.getSite()

    add_catalog_indexes(portal)


def post_install(context):
    """Post install script"""
    # Do something at the end of the installation of this package.


def uninstall(context):
    """Uninstall script"""
    # Do something at the end of the uninstallation of this package.
