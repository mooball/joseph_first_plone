# -*- coding: utf-8 -*-


from Products.CMFCore.interfaces import IDublinCore
from zope.i18nmessageid import MessageFactory
# from plone.directives import form
from plone.autoform.interfaces import IFormFieldProvider
from plone.supermodel import model
from zope.component import adapter
from zope.interface import implementer
from zope.interface import provider
# from zope.i18nmessageid import alsoProvides
from zope import schema


_ = MessageFactory('joseph.theme')


@provider(IFormFieldProvider)
class IPlanets(model.Schema):
    """Add tags to content
    """

    # form.fieldset(
    #     'categorization',
    #     label=_(u'Categorization'),
    #     fields=('planets',),
    # )

    planet = schema.Choice(
        title=_(u'Planet'),
        vocabulary='joseph.theme.Planets',
        required=False
    )


# alsoProvides(IPlanets, IFormFieldProvider)


@implementer(IPlanets)
@adapter(IDublinCore)
class Planets(object):
    """Store tags in the Dublin Core metadata Subject field. This makes
    tags easy to search for.
    """

    def __init__(self, context):
        self.context = context
