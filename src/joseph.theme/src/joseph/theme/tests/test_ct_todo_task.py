# -*- coding: utf-8 -*-
from joseph.theme.content.todo_task import ITodoTask  # NOQA E501
from joseph.theme.testing import JOSEPH_THEME_INTEGRATION_TESTING  # noqa
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


try:
    from plone.dexterity.schema import portalTypeToSchemaName
except ImportError:
    # Plone < 5
    from plone.dexterity.utils import portalTypeToSchemaName


class TodoTaskIntegrationTest(unittest.TestCase):

    layer = JOSEPH_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        portal_types = self.portal.portal_types
        parent_id = portal_types.constructContent(
            'Todo_Folder',
            self.portal,
            'parent_id',
            title='Parent container',
        )
        self.parent = self.portal[parent_id]

    def test_ct_todo_task_schema(self):
        fti = queryUtility(IDexterityFTI, name='Todo Task')
        schema = fti.lookupSchema()
        self.assertEqual(ITodoTask, schema)

    def test_ct_todo_task_fti(self):
        fti = queryUtility(IDexterityFTI, name='Todo Task')
        self.assertTrue(fti)

    def test_ct_todo_task_factory(self):
        fti = queryUtility(IDexterityFTI, name='Todo Task')
        factory = fti.factory
        obj = createObject(factory)

        self.assertTrue(
            ITodoTask.providedBy(obj),
            u'ITodoTask not provided by {0}!'.format(
                obj,
            ),
        )

    def test_ct_todo_task_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.parent,
            type='Todo Task',
            id='todo_task',
        )

        self.assertTrue(
            ITodoTask.providedBy(obj),
            u'ITodoTask not provided by {0}!'.format(
                obj.id,
            ),
        )
