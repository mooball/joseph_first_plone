# -*- coding: utf-8 -*-
from joseph.theme.content.todo_folder import ITodoFolder  # NOQA E501
from joseph.theme.testing import JOSEPH_THEME_INTEGRATION_TESTING  # noqa
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from plone.dexterity.interfaces import IDexterityFTI
from zope.component import createObject
from zope.component import queryUtility

import unittest


try:
    from plone.dexterity.schema import portalTypeToSchemaName
except ImportError:
    # Plone < 5
    from plone.dexterity.utils import portalTypeToSchemaName


class TodoFolderIntegrationTest(unittest.TestCase):

    layer = JOSEPH_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        setRoles(self.portal, TEST_USER_ID, ['Manager'])

    def test_ct_todo_folder_schema(self):
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        schema = fti.lookupSchema()
        self.assertEqual(ITodoFolder, schema)

    def test_ct_todo_folder_fti(self):
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        self.assertTrue(fti)

    def test_ct_todo_folder_factory(self):
        fti = queryUtility(IDexterityFTI, name='Todo Folder')
        factory = fti.factory
        obj = createObject(factory)

        self.assertTrue(
            ITodoFolder.providedBy(obj),
            u'ITodoFolder not provided by {0}!'.format(
                obj,
            ),
        )

    def test_ct_todo_folder_adding(self):
        setRoles(self.portal, TEST_USER_ID, ['Contributor'])
        obj = api.content.create(
            container=self.portal,
            type='Todo Folder',
            id='todo_folder',
        )

        self.assertTrue(
            ITodoFolder.providedBy(obj),
            u'ITodoFolder not provided by {0}!'.format(
                obj.id,
            ),
        )
