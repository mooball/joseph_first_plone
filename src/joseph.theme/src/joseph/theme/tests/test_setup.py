# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from plone.app.testing import setRoles
from plone.app.testing import TEST_USER_ID
from joseph.theme.testing import JOSEPH_THEME_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that joseph.theme is properly installed."""

    layer = JOSEPH_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if joseph.theme is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'joseph.theme'))

    def test_browserlayer(self):
        """Test that IJosephThemeLayer is registered."""
        from joseph.theme.interfaces import (
            IJosephThemeLayer)
        from plone.browserlayer import utils
        self.assertIn(
            IJosephThemeLayer,
            utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = JOSEPH_THEME_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        roles_before = api.user.get_roles(TEST_USER_ID)
        setRoles(self.portal, TEST_USER_ID, ['Manager'])
        self.installer.uninstallProducts(['joseph.theme'])
        setRoles(self.portal, TEST_USER_ID, roles_before)

    def test_product_uninstalled(self):
        """Test if joseph.theme is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'joseph.theme'))

    def test_browserlayer_removed(self):
        """Test that IJosephThemeLayer is removed."""
        from joseph.theme.interfaces import \
            IJosephThemeLayer
        from plone.browserlayer import utils
        self.assertNotIn(
            IJosephThemeLayer,
            utils.registered_layers())
