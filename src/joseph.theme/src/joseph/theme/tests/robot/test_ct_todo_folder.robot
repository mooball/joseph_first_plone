# ============================================================================
# DEXTERITY ROBOT TESTS
# ============================================================================
#
# Run this robot test stand-alone:
#
#  $ bin/test -s joseph.theme -t test_todo_folder.robot --all
#
# Run this robot test with robot server (which is faster):
#
# 1) Start robot server:
#
# $ bin/robot-server --reload-path src joseph.theme.testing.JOSEPH_THEME_ACCEPTANCE_TESTING
#
# 2) Run robot tests:
#
# $ bin/robot /src/joseph/theme/tests/robot/test_todo_folder.robot
#
# See the http://docs.plone.org for further details (search for robot
# framework).
#
# ============================================================================

*** Settings *****************************************************************

Resource  plone/app/robotframework/selenium.robot
Resource  plone/app/robotframework/keywords.robot

Library  Remote  ${PLONE_URL}/RobotRemote

Test Setup  Open test browser
Test Teardown  Close all browsers


*** Test Cases ***************************************************************

Scenario: As a site administrator I can add a Todo Folder
  Given a logged-in site administrator
    and an add Todo Folder form
   When I type 'My Todo Folder' into the title field
    and I submit the form
   Then a Todo Folder with the title 'My Todo Folder' has been created

Scenario: As a site administrator I can view a Todo Folder
  Given a logged-in site administrator
    and a Todo Folder 'My Todo Folder'
   When I go to the Todo Folder view
   Then I can see the Todo Folder title 'My Todo Folder'


*** Keywords *****************************************************************

# --- Given ------------------------------------------------------------------

a logged-in site administrator
  Enable autologin as  Site Administrator

an add Todo Folder form
  Go To  ${PLONE_URL}/++add++Todo Folder

a Todo Folder 'My Todo Folder'
  Create content  type=Todo Folder  id=my-todo_folder  title=My Todo Folder

# --- WHEN -------------------------------------------------------------------

I type '${title}' into the title field
  Input Text  name=form.widgets.IBasic.title  ${title}

I submit the form
  Click Button  Save

I go to the Todo Folder view
  Go To  ${PLONE_URL}/my-todo_folder
  Wait until page contains  Site Map


# --- THEN -------------------------------------------------------------------

a Todo Folder with the title '${title}' has been created
  Wait until page contains  Site Map
  Page should contain  ${title}
  Page should contain  Item created

I can see the Todo Folder title '${title}'
  Wait until page contains  Site Map
  Page should contain  ${title}
