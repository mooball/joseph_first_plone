# -*- coding: utf-8 -*-

from Products.Five import BrowserView
from plone import api
# from DateTime import DateTime
from zope.component import getUtility
from zope.schema.interfaces import IVocabularyFactory


class TodoView(BrowserView):

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def get_search_results(self):
        catalog = api.portal.get_tool('portal_catalog')

        text = self.request.form.get(u'text', None)
        assigned_to = self.request.form.get(u'assigned_to', None)
        color = self.request.form.get(u'color', None)

        sort_on = self.request.form.get(u'sort_on', None)
        sort_order = self.request.form.get(u'sort_order', None)

        query = {
            'portal_type': 'Todo Task',
            'sort_on': None,
            'sort_order': None,
            'sort_limit': 10
        }

        if sort_on:
            query['sort_on'] = sort_on

        if sort_order:
            query['sort_order'] = sort_order

        if text:
            query['SearchableText'] = text

        if assigned_to:
            query['assigned_to'] = assigned_to

        if color:
            query['color'] = color

        result = catalog(query)
        return result

    # def get_user_results(self):
    #     catalog = api.portal.get_tool('portal_catalog')
    #     today = DateTime()
    #     return catalog(
    #         portal_type='Todo Task',
    #         sort_on='effective',
    #         sort_order='reverse',
    #         SearchableText=self.request.form.get(u'text', ''),
    #         assigned_to=self.request.form.get(u'assigned_to', None),
    #         today=today
    #     )

    def get_color_vocabulary(self):
        factory = getUtility(IVocabularyFactory, 'joseph.theme.TaskColors')
        vocabulary = factory(self.context)
        return vocabulary

    def get_user_vocabulary(self):
        factory = getUtility(
            IVocabularyFactory,
            'plone.app.vocabularies.Users')
        vocabulary = factory(self.context)
        return vocabulary
